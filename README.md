
[View Demo](https://flatlogic.github.io/light-blue-react-template/) 


This dashboard is a lite version (containing only few components) of our [Light Blue React](https://flatlogic.com/templates/light-blue-react). There are all ready-to-use components, that are in full version. It is built on the top of React 16, so it well supported along the web. You can easy customize template by changing SCSS variables. All paddings and colors are in _variables.scss file.

## Features

Light Blue React Template is a great template to quick-start development of SAAS, CMS, IoT Dashboard, E-Commerce apps, etc  
Lite version of a Light Blue includes following features and pages:

* Bootstrap 4+ & SCSS
* Responsive layout
* React Chart.js
* Simple login / logout 
* Error page
* Styled Bootstrap components like buttons, modals, etc


## Pages
We have implemented some basic pages, so you can see our template in action.

* Dashboard sample
* Typography
* Tables
* Notifications
* Charts
* Icons
* Maps
* Chat
* Login
* Error page

## Instalation 

1. Clone repository
```shell
git clone https://github.com/flatlogic/light-blue-react-template.git
```
2. Get in the project folder
```shell
cd light-blue-react-template
```
3. Install dependencies via npm or yarn
```shell
npm install
```
```shell
yarn
```

## Quick start
Run development server
```shell
yarn run
```


