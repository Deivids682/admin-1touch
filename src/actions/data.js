import {
  doc,
  collection,
  addDoc,
  query,
  where,
  getDocs,
  onSnapshot,
  updateDoc,
  limit,
  deleteDoc,
} from "firebase/firestore";
import { db } from "../firebase";
import { toast } from "react-toastify";
import _ from "lodash";

export const SEARCH_USERS = "SEARCH_USERS";
export const SET_USER = "SET_USER";
import * as xlsx from "xlsx";
import { create } from "lodash";

export function searchData(searchText) {
  return async (dispatch) => {
    const specialistsRef = collection(db, "specialists");
    const usersRef = collection(db, "user");

    const uid = getDocs(
      query(specialistsRef, where("userId", "==", searchText))
    );
    const name = getDocs(
      query(specialistsRef, where("name", "==", searchText))
    );
    const surname = getDocs(
      query(specialistsRef, where("surname", "==", searchText))
    );
    const phone = getDocs(
      query(specialistsRef, where("phone", "==", searchText))
    );
    const email = getDocs(
      query(
        usersRef,
        where("type", "==", "SPECIALIST"),
        where("email", "==", searchText)
      )
    );

    // Fields dont exist in firestore

    // const licenseId = getDocs(
    //   query(specialistsRef, where('surname', '==', searchText))
    // );
    // const legalId = getDocs(
    //   query(specialistsRef, where('surname', '==', searchText))
    // );

    const companyName = getDocs(
      query(specialistsRef, where("company", "==", searchText))
    );
    const companyAdress = getDocs(
      query(specialistsRef, where("location.streetName", "==", searchText))
    );

    const [
      uidQuerySnapshot,
      nameQuerySnapshot,
      surnameQuerySnapshot,
      phoneQuerySnapshot,
      emailQuerySnapshot,
      companyNameQuerySnapshot,
      companyAdressQuerySnapshot,
    ] = await Promise.all([
      uid,
      name,
      surname,
      phone,
      email,
      companyName,
      companyAdress,
    ]);

    const uidArray = uidQuerySnapshot.docs;
    const namesArray = nameQuerySnapshot.docs;
    const surnameArray = surnameQuerySnapshot.docs;
    const phoneArray = phoneQuerySnapshot.docs;
    const emailArray = emailQuerySnapshot.docs;
    const companyNameArray = companyNameQuerySnapshot.docs;
    const companyAdressArray = companyAdressQuerySnapshot.docs;

    const specialistsArray = uidArray.concat(
      namesArray,
      surnameArray,
      phoneArray,
      emailArray,
      companyNameArray,
      companyAdressArray
    );

    var specialistsData = [];
    specialistsArray.forEach((doc) => {
      const data = doc.data();
      specialistsData.push({
        fullName: `${data.name} ${data.surname}`,
        userId: data.userId,
      });
    });

    if (specialistsArray.length === 0) {
      toast.error(`Error: Found no users`, {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: false,
      });
    } else {
      toast.success(`Success: Found ${specialistsData.length} user(s)`, {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: false,
      });
    }

    dispatch({
      payload: specialistsData,
      type: SEARCH_USERS,
    });
  };
}

export function setAllUsers() {
  return async (dispatch) => {
    const usersRef = collection(db, "user");
    const users = await getDocs(
      query(usersRef, where("type", "==", "SPECIALIST"))
    );

    const specialistsArr = users.docs;
    var specialistsData = [];

    specialistsArr.forEach((doc) => {
      const data = doc.data();
      specialistsData.push({
        fullName: `${data.type}`,
        userId: data.specialistId,
      });
    });

    dispatch({
      payload: specialistsData,
      type: SEARCH_USERS,
    });
  };
}

export function toDateTime(secs) {
  var t = new Date(1970, 0, 1); // Epoch
  t.setSeconds(secs);
  var day = t.getDate();
  var month = t.getMonth();
  var year = t.getFullYear();
  return `${day}.${month < 10 ? `0${month}` : month}.${year}`;
}

export function setData(userId) {
  return async (dispatch) => {
    var data = {};

    const q1 = query(
      collection(db, "specialists"),
      where("userId", "==", userId)
    );
    const unsub1 = onSnapshot(q1, (querySnap) => {
      if (querySnap.docs.length === 0) {
        return toast.error(`Error: User is missing from the database`, {
          position: "top-center",
          autoClose: 3000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: false,
        });
      }
      let queryData = querySnap.docs[0].data();
      data = queryData;
    });

    const q2 = query(collection(db, "user"), where("specialistId", "=="));
    const unsub2 = onSnapshot(q2, (querySnap) => {
      if (querySnap.docs.length === 0) {
        return toast.error(`Error: User is missing from the database`, {
          position: "top-center",
          autoClose: 3000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: false,
        });
      }
      let queryData = querySnap.docs[0].data();
      data = {
        ...data,
        email: queryData.email,
        emailVerified: queryData.emailVerified,
        registrationDate: toDateTime(queryData.registrationDate.seconds),
        type: queryData.type,
      };
      dispatch({
        payload: data,
        type: SET_USER,
      });
    });
  };
}

export async function editUserData(userId, data) {
  const q = query(
    collection(db, "user"),
    where("specialistId", "==", userId),
    limit(1)
  );
  const querySnapshot = await getDocs(q);
  const doc = querySnapshot.docs[0];

  try {
    await updateDoc(doc.ref, {
      ...data,
    });
    return toast.success(`Success: Data updated`, {
      position: "top-center",
      autoClose: 2000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: false,
    });
  } catch (err) {
    console.error(err);
    return toast.error(`Error: Something went wrong`, {
      position: "top-center",
      autoClose: 2000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: false,
    });
  }
}

export async function editSpecialistData(userId, data) {
  const q = query(
    collection(db, "specialists"),
    where("userId", "==", userId),
    limit(1)
  );
  const querySnapshot = await getDocs(q);
  const doc = querySnapshot.docs[0];

  try {
    await updateDoc(doc.ref, {
      ...data,
    });
    return toast.success(`Success: Data updated`, {
      position: "top-center",
      autoClose: 2000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: false,
    });
  } catch (err) {
    console.error(err);
    return toast.error(`Error: Something went wrong`, {
      position: "top-center",
      autoClose: 2000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: false,
    });
  }
}


const compareDates = (a, b) => {
  if (a.registrationDate && b.registrationDate) 
      return a.registrationDate.seconds > b.registrationDate.seconds ? -1 : 1;
  else if (a.registrationDate === undefined && b.registrationDate === undefined)
      return 0;
  else 
      return a.registrationDate === undefined ? 1 : -1;
}

export const getUserData = async () => {

  //takes both collections from Firebase, 

  //finds matches between users with type SPECIALIST and specialists

  //and merges their data to be used later

  let userData = await getCollectionData('user');

  let specData = await getCollectionData('specialists');

  return userData.map((user) => {
    if (user.specialistId) {
      const matchedSpec = _.find(
        specData,
        (spec) => spec.userId == user.specialistId
      );
      return matchedSpec ? { ...user, ...matchedSpec } : user;
    } else return user;

  }).sort(compareDates);

};


export const exportData = async () => {
  let wb = xlsx.utils.book_new();
  wb.Props = { Title: "1touch user data", Author: "1TOUCH" };
  let ws = createWorksheetSample();
  let filledWs = await exportUsersData(ws);
  xlsx.utils.book_append_sheet(wb, filledWs, "Users");
  xlsx.writeFile(wb, 'userData.xlsx');
};

export const exportUsersData = async (ws) => {
  let categories = await getCollectionData("categories-new");
  let userData = await getUserData();
  userData.sort(compareDates).forEach((user) => {
    const {
      type,
      name,
      surname,
      email,
      phone,
      endDate,
      registrationDate,
      checkedReceiveMarketing
    } = user || '';

    let formattedDate = '';
    if(registrationDate)
        formattedDate = new Date(registrationDate.seconds*1000);
    const promoCode = user?.license?.promoCode || '';
    const subCategory = user?.subCategory
      ? user.subCategory.flatMap((userCat) =>
          categories
            .filter((category) =>
              userCat == "MAP"
                ? false
                : userCat == category.group + "_" + category.categoryTwoEnum
                ? true
                : false
            )
            .map((filteredCat) => filteredCat.categoryTwo)
        ).join(', ')
      : '';

    //we need to process array-like properties separately

    xlsx.utils.sheet_add_aoa(
      ws,
      [
        [
          type,
          name,
          surname,
          email,
          phone,
          subCategory,
          promoCode,
          checkedReceiveMarketing ? 'Jā' : 'Nē',
          formattedDate,
          endDate,
        ],
      ],
      { origin: -1,
        dateNF: 'DD"."MM"."YYYY hh":"mm',
        cellDates: true 
      }
    ); //adds a row with data to an exsel sheet
  });
  return ws;
};

export const getCollectionData = async (collectionName, docNameRequired = false, docRefRequired = false) => {
  const collectionRef = collection(db, collectionName);
  const collectionDocs = await getDocs(query(collectionRef));
  return collectionDocs.docs.map((doc) => {
    let result = doc.data();
    if (docNameRequired) result.firebaseDocId = doc.id;
    if (docRefRequired) result.firebaseDocRef = doc.ref;
    return result;
  })
}


export const createWorksheetSample = () => {
  /* i create the file here since it is stated in XLSX docs that
  readFile method can throw an error in most browsers due to 
  security risks
  */

  let worksheet = xlsx.utils.aoa_to_sheet([
    [
      "Lietotāja tips",
      "Vārds",
      "Uzvārds",
      "E-pasts",
      "Telefons",
      "Pak. kategorijas",
      "Promo kods",
      "Piekr. mārketinga sūtījumiem",
      "Reģistrācijas datums",
      "Beigu datums",
    ],
  ]);

  const cols = [
    { wch: 14 }, //Lietotāja tips
    { wch: 10 }, //Vārds
    { wch: 10 }, //Uzvārds
    { wch: 15 }, //E-pasts
    { wch: 15 }, //Telefons
    { wch: 30 }, //Pak. kategorijas
    { wch: 15 }, //Promo kods
    { wch: 5 }, //Piekr. mārketinga sūtījumiem
    { wch: 14 }, //Reģistrācijas datums
    { wch: 14 }, //Beigu datums
    //wch - width in characters
  ];
  worksheet["!cols"] = cols;

  return worksheet;
};

export const addErrorLogToSortedLogsCollection = async (error) => {
  let docId = '';
  await addDoc(collection(db, "sorted-logs"), 
  {
    message: error.message,
    oldCount: error.newCount,
    isSolved: error.isSolved,
    solvedDate: error.solvedDate,
    lastTimeOccurred: error.lastTimeOccured
  }).then((docRef)=>{docId = docRef.id});
  return docId;
  
  //can be destructured with {a, b, c} = error, but it wouldnt make code shorter
}

export const updateSortedLog = async (log, docId) => { 
  await updateDoc(doc(db, "sorted-logs", docId), log);
}

export const deleteSolvedErrorLog = async (errorDoc) => {
  deleteDoc(doc(db, "sorted-logs", errorDoc.firebaseDocId))
 };
