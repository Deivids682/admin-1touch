import React, { useState, useEffect } from "react";
import { Badge, Button } from "reactstrap";
import {
  addErrorLogToSortedLogsCollection,
  updateSortedLog,
  getCollectionData,
  deleteSolvedErrorLog,
} from "../../actions/data";
import s from "./ErrorTable.module.scss";

const ErrorTable = () => {
  const [errorLogs, setErrorLogs] = useState();
  const [isLoaded, setIsLoaded] = useState(false);

  const __setErrorLogs = async () => {
    const errorLogs = getCollectionData("logs", true);
    const sortedErrorLogs = getCollectionData("sorted-logs", true, true);
    Promise.all([errorLogs, sortedErrorLogs]).then((values) => {
      //promise.all allows calling both collections simultaneously
      setErrorLogs({
        unsorted: values[0],
        sorted: values[1],
      });
    });
  };

  useEffect(() => {
    __setErrorLogs();
  }, []);


  const getTableRows = () => {
    if (!errorLogs) return "";
    let tableRows = processErrorCollections().map((log, i) => {
      let shortText =
        log.message.length > 110 ? log.message.slice(0, 110) : false;
      return (
        <tr key={i}>
          {shortText ? ( //error message
            <td onClick={(e) => toggleExpand(e, shortText, log.message)}>
              {shortText}
            </td>
          ) : (
            <td>{log.message}</td>
          )}

          <td className={s.isSolved}>
            {log.isSolved ? (
              <Button
                outline color="success"
                size="sm"
                onClick={() => {
                  solveProblemHandler(log, i);
                }}
              >
                YES (solve again)
              </Button>
            ) : (
              <Button
                outline color="info"
                onClick={() => {
                  solveProblemHandler(log, i);
                }}
              >
                SOLVE NOW
              </Button>
            )}
          </td>
          <td className={s.count}>{log.newCount}</td>
          <td className={s.occurrences}>
            <Badge
              color={
                log.newCount > 20
                  ? "danger"
                  : log.newCount > 10
                  ? "warning"
                  : "success"
              }
            >
              {log.newCount > 20 ? "A lot" : log.newCount > 10 ? "Many" : "Few"}
            </Badge>
          </td>
          <td>{`${log.lastTimeOccured.getDate()}.${
            log.lastTimeOccured.getMonth() + 1
          }.${log.lastTimeOccured.getFullYear()} ${log.lastTimeOccured.getHours()}:${String(
            log.lastTimeOccured.getMinutes()
          ).padStart(2, "0")}`}</td>
          <td className={s.solvedDate}>
            {" "}
            {log.solvedDate || <Badge color="dark">Not solved</Badge>}
          </td>
          <td>
            {log.oldCount == 0 ? (
              <Badge color="danger">New!</Badge>
            ) : (
              log.oldCount
            )}
          </td>
          <td>
            {log.isSolved ? (
              log.isRecurring ? (
                <Badge color="danger">Yes</Badge>
              ) : (
                <Badge color="secondary">No</Badge>
              )
            ) : (
              <Badge color="dark">Not solved</Badge>
            )}
          </td>
        </tr>
      );
    });
    if (!isLoaded) setIsLoaded(true);
    return tableRows;
  };

  const toggleExpand = (e, shortText, longText) =>
    (e.target.textContent =
      e.target.textContent == shortText ? longText : shortText);

  const processErrorCollections = () => {
    let { errorCounts, latestTimestamps } = processErrorLogsCollection();
    let result = processSortedErrorLogsCollection(
      errorCounts,
      latestTimestamps
    );
    console.log("result", result);
    return result;
  };

  const processErrorLogsCollection = () => {
    let errorCounts = {};
    let latestTimestamps = {};
    console.log(errorLogs);
    errorLogs.unsorted.forEach((x) => {
      errorCounts[x.message] = (errorCounts[x.message] || 0) + 1;
      latestTimestamps[x.message] = latestTimestamps[x.message]
        ? getLatestDate(
            new Date(x.firebaseDocId.slice(0, 26)),
            latestTimestamps[x.message]
          )
        : new Date(x.firebaseDocId.slice(0, 26));
    });
    return { errorCounts, latestTimestamps };
  };

  const getLatestDate = (a, b) => (a > b ? a : b);

  const processSortedErrorLogsCollection = (errorCounts, latestTimestamps) => {
    let combinedData = [];
    errorLogs.sorted.forEach((log) => {
      let occurrences = errorCounts[log.message] || false;
      if (occurrences) {
        let formattedSolvedDate = null;
        if (log.solvedDate) {
          let solvedDateInUTC = log.solvedDate.toDate();
          console.log(solvedDateInUTC);
          solvedDateInUTC.setTime(
            solvedDateInUTC.getTime() + 3 * 60 * 60 * 1000
          ); //Latvian timezone support
          formattedSolvedDate = `${solvedDateInUTC.getUTCDate()}.${
            solvedDateInUTC.getUTCMonth() + 1
          }.${solvedDateInUTC.getUTCFullYear()} ${solvedDateInUTC.getUTCHours()}:${solvedDateInUTC.getUTCMinutes()}`;
        }
        combinedData.push({
          firebaseDocId: log.firebaseDocId,
          message: log.message,
          oldCount: log.oldCount,
          newCount: occurrences,
          isSolved: log.isSolved,
          solvedDate: formattedSolvedDate,
          isRecurring: log.oldCount < occurrences,
          lastTimeOccured: latestTimestamps[log.message] || null,
        });

        delete errorCounts[log.message];
        delete latestTimestamps[log.message];
      } else deleteSolvedErrorLog(log);
    });
    let newErrors = Object.keys(errorCounts);
    if (newErrors.length > 0)
      newErrors.forEach(async (message) => {
        let newError = {
          message: message,
          oldCount: 0,
          newCount: errorCounts[message],
          isSolved: false,
          solvedDate: null,
          isRecurring: false,
          lastTimeOccured: latestTimestamps[message],
        };
        let docId = await addErrorLogToSortedLogsCollection(newError);
        newError.firebaseDocId = docId;
        combinedData.push(newError);
      });
    return combinedData.sort((a, b) => b.newCount - a.newCount);
  };

  const solveProblemHandler = async (log) => {
    log.isSolved = true;
    log.solvedDate = new Date();
    log.oldCount = log.newCount;
    let firebaseDocId = log.firebaseDocId;
    delete log[firebaseDocId];
    await updateSortedLog(log, firebaseDocId);
    __setErrorLogs();
    //i use that to re-render component and show corresponding badges, but its not neccessary
  };

  return (
    <div  className={s.errorTableWrapper} style={{'display' : isLoaded ? 'block' : 'none'}}>
    <table className={s.errorTable}>
      <tbody>
        <tr className={s.title}>
          <th className={s.description}>error message</th>
          <th className={s.errorStatus}>problem solved? </th>
          <th className={s.count}>count</th>
          <th className={s.occurences}>occurrences</th>
          <th className={s.lastOccurence}>last time occured</th>
          <th className={s.solvedDate}>problem solved date</th>
          <th className={s.prevCount}>prev count</th>
          <th className={s.didReoccur}>did reoccur</th>
        </tr>
        {getTableRows()}
      </tbody>
    </table>
    </div>
  );
};

export default ErrorTable;
