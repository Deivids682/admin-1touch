// Table.js

import { Button } from "reactstrap";
import React, { useEffect, useMemo, useState } from "react";
import { useTable, useFilters, useSortBy, usePagination } from "react-table";
import { ColumnFilter } from "./ColumnFilter";
import s from "./Table.module.scss";
export default function PaginationTable({ columns, data }) {
  const defaultColumn = useMemo(() => {
    return {
      Filter: ColumnFilter,
    };
  });

  const {
    getTableProps, // table props from react-table
    getTableBodyProps, // table body props from react-table
    headerGroups, // headerGroups, if your table has groupings
    page, // page for the table based on the data passed
    nextPage,
    previousPage,
    canNextPage,
    canPreviousPage,
    state,
    pageCount,
    pageSize,
    setPageSize,
    gotoPage,
    prepareRow, // Prepare the row (this function needs to be called for each row before getting the row props)
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
    },
    useFilters,
    useSortBy,
    usePagination
  );

  const { pageIndex } = state;
  const [active, setActive] = useState(0);
  const [isLoaded, setIsLoaded] = useState(false);

  
  useEffect(() => {
    if (!isLoaded) {
      if (page.length > 0) setIsLoaded(true);
    }
  }, [page]);

  const previousPageHandler = () => {
    previousPage();
    setActive(pageIndex - 1);
  };

  const gotoPageHandler = (i) => {
    gotoPage(i);
    setActive(i);
  };

  const nextPageHandler = () => {
    nextPage();
    setActive(pageIndex + 1);
  };

  const redirectHandler = (id, type) => {
    if (type === "SPECIALIST" || type === "CLIENT") {
      window.open(`/#/users/${id}`);
    }
  };

  return (
    <div style={{'display': isLoaded ? 'block' : 'none'}}>
      <table className={s.reactTable} {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th scope="col">
                  <div
                    {...column.getHeaderProps(column.getSortByToggleProps)}
                    className={s.columnTitle}
                  >
                    {column.render("Header")}
                    <span>
                      {column.isSorted ? (column.isSortedDesc ? "↓" : "↑") : ""}
                    </span>
                  </div>
                  <div>
                    <div>{column.render("Filter")}</div>
                  </div>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((row) => {
            prepareRow(row);
            return (
              <tr
                {...row.getRowProps()}
                onClick={(e) => {
                  redirectHandler(row.id, row.original.type);
                }}
              >
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className={s.tableFooter}>
        <select
          className={s.pageSizeSelect}
          value={pageSize}
          onChange={(e) => setPageSize(Number(e.target.value))}
        >
          {[10, 25, 50].map((pageSize) => (
            <option
              className={s.pageSizeOption}
              key={pageSize}
              value={pageSize}
            >
              {pageSize}
            </option>
          ))}
        </select>
        <div className={s.tableNavigation}>
          <Button
            outline
            color="info"
            size="sm"
            disabled={!canPreviousPage}
            onClick={() => previousPageHandler()}
          >
            &#60;
          </Button>
          {[...Array(pageCount)].map((x, i) => (
            <Button
              outline
              color="info"
              size="sm"
              active={active === i}
              onClick={() => gotoPageHandler(i)}
            >
              {i + 1}
            </Button>
          ))}
          <Button
            outline
            color="info"
            size="sm"
            disabled={!canNextPage}
            onClick={() => nextPageHandler()}
          >
            &#62;
          </Button>
        </div>
      </div>
    </div>
  );
}
