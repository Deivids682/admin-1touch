import React, { useMemo, useState, useEffect } from "react";
import { getUserData, exportData } from "../../actions/data";
import { Button } from "reactstrap";
import s from "./UserTable.module.scss";
import Table from "../Table/TableWithRedirect";
const UserTable = () => {
  const [userData, setUserData] = useState([]);

  const columns = useMemo(
    () => [
      {
        Header: "First Name",
        accessor: "name",
      },
      {
        Header: "Last Name",
        accessor: "surname",
      },
      {
        Header: "User type",
        accessor: (row) => {return row.type == 'SPECIALIST_ADD' ? 'PASSIVE_CLIENT' : row.type},
      },
      {
        Header: "Phone",
        accessor: "phone",
      },
      {
        Header: "e-mail",
        accessor: "email",
      },
      {
        Header: "Account status",
        accessor: "emailVerified",
      },
    ],
    []
  );

  useEffect(() => {
    async function __getDocs() {
      const users = await getUserData();
      setUserData(users);
    }
    __getDocs();
  }, []);

  return (
    <div className={s.userTableWrapper}>
      <Button
        color="default"
        className={s.download}
        size="sm"
        onClick={exportData}
      >
        Download user data
      </Button>
      <div className={s.userTable}>
        <Table columns={columns} data={userData} />
      </div>
    </div>
  );
};

export default UserTable;
