import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
const firebaseConfig = {
  production: {
    apiKey: "AIzaSyDLooALLCaPgBA0cEHTuLni5fXn0KHa9zo",
    authDomain: "touchapp-6ae7c.firebaseapp.com",
    databaseURL: "https://touchapp-6ae7c.firebaseio.com",
    projectId: "touchapp-6ae7c",
    storageBucket: "touchapp-6ae7c.appspot.com",
    messagingSenderId: "146160415422",
    appId: "1:146160415422:web:abf151ac99361ff870e4ef",
    measurementId: "G-BGSSG8D4KW"
  },
  development: {
    apiKey: "AIzaSyC6gxL1Lwi9GwAs5a75NZ_fXl1Z3HwVVk8",
    authDomain: "touchtest-bc1a7.firebaseapp.com",
    projectId: "touchtest-bc1a7",
    storageBucket: "touchtest-bc1a7.appspot.com",
    messagingSenderId: "34714399413",
    appId: "1:34714399413:web:9a55fb6b25513d650744fc",
    measurementId: "G-QF3NBTGZYS"
  }
};
const app = initializeApp(firebaseConfig[process.env.NODE_ENV]);
export const db = getFirestore(app);
