import React, { useState } from "react";
import Header from "../../components/Header/Header";
import Sidebar from "../../components/Sidebar/Sidebar";
import s from "./CreateInvoices.module.scss";
import * as xlsx from "xlsx";
import Loader from "../../components/Loader/Loader";

const CreateInvoices = () => {
  const [jsonsList, setJsonsList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [responseType, setResponseType] = useState();

  const readUploadFile = (e) => {
    e.preventDefault();
    if (e.target.files) {
      const reader = new FileReader();
      reader.onload = (e) => {
        const data = e.target.result;
        const workbook = xlsx.read(data, { type: "array" },);
        const sheetName = workbook.SheetNames[0];
        const worksheet = workbook.Sheets[sheetName];
        const json = xlsx.utils.sheet_to_json(worksheet);
        setJsonsList(json)
      };
      reader.readAsArrayBuffer(e.target.files[0]);
    }
  }

  const sendInvoices = async () => {
    if (jsonsList.length > 0) {
      setIsLoading(true)
      fetch('https://us-central1-touchapp-6ae7c.cloudfunctions.net/sendStripeInvoicesManually', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ items: jsonsList }),
      }).then(async (response) => {
        setIsLoading(false);
        setResponseType("success");
      }).catch((error) => {
        setIsLoading(false)
        setResponseType("error");
      });
    }

  }

  return (
    <div className={s.wrap}>
      <Header />
      <Sidebar />
      <main className={s.content}>
        <h2 className="mb-3"> Izveido un nosūti Stripe rēķinus </h2>
        {isLoading && <Loader size={80} />}
        {!isLoading && !responseType && <div>
          <input type="file" name="upload" id="upload" onChange={readUploadFile} accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
          {jsonsList.length > 0 && <button class="btn btn-success animated wobble" onClick={() => sendInvoices()}>Nosūtīt rēķinus</button>}
          {jsonsList.length > 0 &&
            <table class="table mt-5">
              <thead>
                <tr>
                  <th scope="col" className="text-center">#</th>
                  <th scope="col" className="text-center">Full name</th>
                  <th scope="col" className="text-center">City</th>
                  <th scope="col" className="text-center">Country</th>
                  <th scope="col" className="text-center">Language</th>
                  <th scope="col" className="text-center">Tax Rate Id</th>
                  <th scope="col" className="text-center">Email</th>
                  <th scope="col" className="text-center">Phone</th>
                  <th scope="col" className="text-center">Service title</th>
                  <th scope="col" className="text-center">UnitPrice</th>
                  <th scope="col" className="text-center">Quantity</th>
                  <th scope="col" className="text-center">Currency</th>
                  <th scope="col" className="text-center">Days Until Due</th>
                  <th scope="col" className="text-center">FooterText</th>
                </tr>
              </thead>
              <tbody>
                {jsonsList.map((item, index) => <tr>
                  <th scope="row" className="text-center">{index + 1}</th>
                  <td className="text-center">{item.FullName}</td>
                  <td className="text-center">{item.City}</td>
                  <td className="text-center">{item.Country}</td>
                  <td className="text-center">{item.Language}</td>
                  <td className="text-center">{item.TaxRateId}</td>
                  <td className="text-center">{item.Email}</td>
                  <td className="text-center">{item.Phone}</td>
                  <td className="text-center">{item.ServiceTitle}</td>
                  <td className="text-center">{item.UnitPrice}</td>
                  <td className="text-center">{item.Quantity}</td>
                  <td className="text-center">{item.Currency}</td>
                  <td className="text-center">{item.DaysUntilDue}</td>
                  <td className="text-center">{item.FooterText}</td>
                </tr>)}
              </tbody>
            </table>
          }
        </div>
        }
        {
          responseType && <h3 className={responseType == 'success' ? 'text-success' : 'text-danger'} >{responseType == "success" ? "Stripe rēķini veiksmīgi nosūtīti" : "Ir notikusi kļūda lūdzu. Lūdzu pārlādē lapu!"}</h3>
        }
      </main>
    </div >
  );
};

export default CreateInvoices;
