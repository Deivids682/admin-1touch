import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Row, Col } from "reactstrap";

import Widget from "../../components/Widget";

import s from "./Dashboard.module.scss";

import ServiceArea from "./components/ServiceArea";
import Services from "./components/Services";
import BasicInfo from "./components/BasicInfo";
import LegalInfo from "./components/LegalInfo";
import LicenseInfo from "./components/LicenseInfo";

import { editUserData } from "../../actions/data";

class Dashboard extends React.Component {
  static propTypes = {
    selectedUser: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      graph: null,
      checkedArr: [false, false, false],
    };
    this.checkTable = this.checkTable.bind(this);
    this.handleEditUserData = this.handleEditUserData.bind(this);
  }

  handleEditUserData(type) {
    this.props.dispatch(
      editUserData("user", this.props.selectedUser.userId, {
        type: type,
      })
    );
  }

  checkTable(id) {
    let arr = [];
    if (id === 0) {
      const val = !this.state.checkedArr[0];
      for (let i = 0; i < this.state.checkedArr.length; i += 1) {
        arr[i] = val;
      }
    } else {
      arr = this.state.checkedArr;
      arr[id] = !arr[id];
    }
    if (arr[0]) {
      let count = 1;
      for (let i = 1; i < arr.length; i += 1) {
        if (arr[i]) {
          count += 1;
        }
      }
      if (count !== arr.length) {
        arr[0] = !arr[0];
      }
    }
    this.setState({
      checkedArr: arr,
    });
  }

  render() {
    const { selectedUser } = this.props;
    if (selectedUser === null) return <></>;

    return (
      <div className={s.root}>
        <h1 className="page-title">Dashboard</h1>

        <Row>
          <Col lg={5}>
            <Widget title={<h6> Basic </h6>}>
              <BasicInfo
                selectedUser={selectedUser}
                editUserData={this.handleEditUserData}
              />
            </Widget>
          </Col>
          <Col lg={5}>
            <Widget title={<h6> Legal </h6>}>
              <LegalInfo selectedUser={selectedUser} />
            </Widget>
          </Col>
          <Col lg={2}>
            <Widget title={<h6> Service area </h6>}>
              <ServiceArea selectedUser={selectedUser} />
            </Widget>
            <Widget title={<h6> Services </h6>}>
              <Services selectedUser={selectedUser} />
            </Widget>
          </Col>
        </Row>

        <Row>
          <Col lg={12}>
            <Widget title={<h6> License info </h6>}>
              <LicenseInfo selectedUser={selectedUser} />
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(store) {
  return {
    selectedUser: store.data.currentUser,
  };
}

  export default connect(mapStateToProps)(Dashboard);
