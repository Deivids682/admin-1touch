import React, { Component } from "react";
import { Col, Row } from "reactstrap";

export class LicenseInfo extends Component {
  render() {
    const { selectedUser } = this.props;
    return (
      <Row>
        <Col>
          <table className="table-striped table">
            <tbody>
              <tr>
                <td>License ID</td>
                <td></td>
              </tr>
              <tr>
                <td>License status</td>
                <td></td>
              </tr>
              <tr>
                <td>License updated</td>
                <td>{selectedUser.location.streetName}</td>
              </tr>
              <tr>
                <td>Billing date</td>
                <td></td>
              </tr>
              <tr>
                <td>Ballance</td>
                <td>{selectedUser.location.city}</td>
              </tr>
            </tbody>
          </table>
        </Col>
        <Col>
          <table className="table-striped table">
            <tbody>
              <tr>
                <td>License ID</td>
                <td></td>
              </tr>
              <tr>
                <td>License status</td>
                <td></td>
              </tr>
              <tr>
                <td>License updated</td>
                <td>{selectedUser.location.streetName}</td>
              </tr>
              <tr>
                <td>Billing date</td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </Col>
        <Col>
          <table className="table-striped table">
            <tbody>
              <tr>
                <td>License ID</td>
                <td></td>
              </tr>
              <tr>
                <td>License status</td>
                <td></td>
              </tr>
              <tr>
                <td>License updated</td>
                <td>{selectedUser.location.streetName}</td>
              </tr>
              <tr>
                <td>Billing date</td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </Col>
        <Col>
          <table className="table-striped table">
            <tbody>
              <tr>
                <td>License ID</td>
                <td></td>
              </tr>
              <tr>
                <td>License status</td>
                <td></td>
              </tr>
              <tr>
                <td>License updated</td>
                <td>{selectedUser.location.streetName}</td>
              </tr>
              <tr>
                <td>Billing date</td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </Col>
      </Row>
    );
  }
}

export default LicenseInfo;
