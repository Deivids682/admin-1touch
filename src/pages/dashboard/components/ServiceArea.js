import React, { Component } from "react";

const serviceAreas = ["Skaistumkopšana", "Sports", "Veselība"];

export class ServiceArea extends Component {
  render() {
    return (
      <>
        {serviceAreas.map((serviceArea, key) => (
          <div className="stats-row" key={key}>
            <div className="abc-checkbox">
              <input
                readOnly
                checked={
                  serviceArea ===
                  this.props.selectedUser.area.find((e) => e === serviceArea)
                }
                id={`${serviceArea}-checkbox`}
                type="checkbox"
                className="form-check-input"
              />
              <label htmlFor={`${serviceArea}-checkbox`} className="">
                {serviceArea}
              </label>
            </div>
          </div>
        ))}
      </>
    );
  }
}

export default ServiceArea;
