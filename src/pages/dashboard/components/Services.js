import React, { Component } from "react";
import serivceTypeToString from "./util/serviceTypeToString";

const services = [
  "HAIR_DRESSER",
  "BARBER",
  "MANICURE",
  "PEDICURE",
  "EYBROWS_EYELASHES",
];

export class Services extends Component {
  render() {
    return (
      <>
        {services.map((service, key) => (
          <div className="stats-row" key={key}>
            <div className="abc-checkbox">
              <input
                readOnly
                checked={
                  service ===
                  this.props.selectedUser.categories.find((e) => e === service)
                }
                id={`${service}-checkbox`}
                type="checkbox"
                className="form-check-input"
              />
              <label htmlFor={`${service}-checkbox`} className="">
                {serivceTypeToString(service)}
              </label>
            </div>
          </div>
        ))}
      </>
    );
  }
}

export default Services;
