export default function serivceTypeToString(type) {
  switch (type) {
    case "HAIR_DRESSER":
      return "Frizieris";
    case "BARBER":
      return "Bārddzinis";
    case "MANICURE":
      return "Manikīrs";
    case "PEDICURE":
      return "Pedikīrs";
    case "EYBROWS_EYELASHES":
      return "Skropstas un uzacis";
    case "COSMETOLOGIST":
      return "Kosmetologs";
    case "MASAGE":
      return "Masāža"
    case "TATTOO":
      return "Tattoo un pirsings";
    case "MAKEUP":
      return "Make-up";
    default:
      break;
  }
}

// enum GroupTypes {
//     HAIRE_DRESSER,
//     MANICURE,
//     MAKEUP,
//     TATTOO,
//     BARBER,
//     COSMETOLOGIST,
//     PEDICURE,
//     MASAGE,
//     EYBROWS_EYELASHES,
//     NONE
//   }
