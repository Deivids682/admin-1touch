import React from "react";
import s from "./UserInfo.module.scss";
import { getUserData } from "../../actions/data";

import { Row, Col } from "reactstrap";
import BasicInfo from "./components/BasicInfo";
import LegalInfo from "./components/LegalInfo";
import Widget from "../../components/Widget/Widget";
import ServiceArea from "./components/ServiceArea";
import Services from "./components/Services";
class UserInfo extends React.Component {
  constructor(props) {
    super();
    this.state = {
      id: props.match.params.id,
      user: false,
    };
  }

  componentDidMount = async () => {
    const userData = await getUserData();
    console.log(userData);
    this.setState({ user: userData[this.state.id] });
  };



  render() {

    if (!this.state.user) {
      return <div>Loading...</div>;
    }

    return (
      <div className={s.userInfoWrapper}>
        <div className={s.title}>USER INFO</div> 
        <Row>
          <Col lg={5}>
            <Widget title={<h6> Basic </h6>}>
              <BasicInfo
                selectedUser={this.state.user}
                editUserData={this.handleEditUserData}
              />
            </Widget>
          </Col>
          {this.state.user.type == "SPECIALIST" ? specialistWidgets(this.state.user) : ""}
        </Row>
      </div>
    );
  }
}

export default UserInfo;


const specialistWidgets = (user) => 
<>
  <Col lg={5}>
    <Widget title={<h6> Legal </h6>}>
      <LegalInfo selectedUser={user} />
    </Widget> 
  </Col>
  <Col lg={2}>
    <Widget title={<h6> Service area </h6>}>
        <ServiceArea selectedUser={user} /> 
    </Widget>
    <Widget title={<h6> Services </h6>}>
        <Services selectedUser={user} /> 
    </Widget>
  </Col>
</>