import React, { Component } from "react";
import {Badge} from "reactstrap";
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  Input,
  UncontrolledButtonDropdown,
} from "reactstrap";

import { editUserData, toDateTime } from "../../../actions/data";

export class BasicInfo extends Component {
  constructor(props) {
    super(props);
    this.toggleTypeDropdown = this.toggleTypeDropdown.bind(this);
    this.handleEditField = this.handleEditField.bind(this);
    this.handleEditType = this.handleEditType.bind(this);
    this.handleEditPhone = this.handleEditPhone.bind(this);

    this.state = {
      typeDropdownOpen: false,
      phoneNumber: this.props.selectedUser.phone,
    };
  }

  toggleTypeDropdown() {
    this.setState({
      typeDropdownOpen: !this.state.typeDropdownOpen,
    });
  }

  handleEditType(type) {
    editUserData(this.props.selectedUser.userId, { type: type });
  }

  handleEditPhone(e) {
    e.preventDefault();
    editUserData(this.props.selectedUser.userId, {
      phone: this.state.phoneNumber,
    });
  }

  handleEditField(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  render() {
    const { selectedUser } = this.props;
    console.log(selectedUser);
    return (
      <table className="table-striped table">
        <tbody>
          <tr>
            <td>User ID</td>
            <td>{selectedUser.userId || selectedUser.specialistId}</td>
          </tr>
          <tr>
            <td>Account status</td>
            <td>active</td>
          </tr>
          <tr>
            <td>User type</td>
            <td>
              <UncontrolledButtonDropdown>
                <DropdownToggle
                  color="inverse"
                  className="mr-xs"
                  size="sm"
                  caret
                  disabled 
                >
                  {selectedUser.type}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem
                    onClick={this.handleEditType.bind(this, "SPECIALIST")}
                  >
                    SPECIALIST
                  </DropdownItem>
                  <DropdownItem
                    onClick={this.handleEditType.bind(this, "OTHER")}
                  >
                    OTHER
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledButtonDropdown>
            </td>
          </tr>
          <tr>
            <td>First name</td>
            <td>{selectedUser.name}</td>
          </tr>
          <tr>
            <td>Last name</td>
            <td>{selectedUser.surname}</td>
          </tr>
          <tr>
            <td>Phone</td>
            <td>
              <Form style={{position: 'relative'}}onSubmit={this.handleEditPhone}>
                <Input
                  name="phoneNumber"
                  onChange={this.handleEditField}
                  value={this.state.phoneNumber}
                  className="input-transparent"
                />
                {selectedUser.phone ? (
                  <Badge style={{position: 'absolute', right: '10px', top:'7px'}} color="success">Verified</Badge>
                ) :  (
                  <Badge style={{position: 'absolute', right: '10px', top:'7px'}} color="danger">Not verified</Badge>
                )}
              </Form>
            </td>

          </tr>
          <tr>
            <td>E-mail</td>
            <td>
              {" "}
              {selectedUser.email}{""}
              {selectedUser.emailVerified ? (
                 <Badge style={{float: 'right'}} color="success">Verified</Badge>
              ) : (
                <Badge style={{float: 'right'}} color="danger">Not verified</Badge>
                )}
            </td>
          </tr>
          <tr>
            <td>Account start</td>
            <td>{selectedUser.registrationDate ? toDateTime(selectedUser.registrationDate.seconds) : ""}</td>
          </tr>
          <tr>
            <td>Account end</td>
            <td></td>
          </tr>
          <tr>
            <td>License ID</td>
            <td></td>
          </tr>
        </tbody>
      </table>
    );
  }
}

export default BasicInfo;
