import React, { Component } from "react";

export class LegalInfo extends Component {
  render() {
    const { selectedUser } = this.props;
    return (
      <table className="table-striped table">
        <tbody>
          <tr>
            <td>Legal ID</td>
            <td></td>
          </tr>
          <tr>
            <td>Company name</td>
            <td>{selectedUser.company}</td>
          </tr>
          <tr>
            <td>Adress line 1</td>
            <td>{selectedUser.location?.streetName || null}</td>
          </tr>
          <tr>
            <td>Adress line 2</td>
            <td></td>
          </tr>
          <tr>
            <td>City</td>
            <td>{selectedUser.location?.city}</td>
          </tr>
          <tr>
            <td>State/Province</td>
            <td>{selectedUser.location?.province}</td>
          </tr>
          <tr>
            <td>Country</td>
            <td>{selectedUser.location?.country}</td>
          </tr>
        </tbody>
      </table>
    );
  }
}

export default LegalInfo;
