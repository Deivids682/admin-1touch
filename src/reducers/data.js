import { SEARCH_USERS, SET_USER } from "../actions/data";

export default function data(
  state = {
    users: [],
    currentUser: null,
  },
  action
) {
  switch (action.type) {
    case SEARCH_USERS:
      return Object.assign({}, state, {
        users: action.payload,
      });
    case SET_USER:
      return Object.assign({}, state, {
        currentUser: action.payload,
      });
    default:
      return state;
  }
}
